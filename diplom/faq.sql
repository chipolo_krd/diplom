-- MySQL Script generated by MySQL Workbench
-- 09/13/16 11:54:03
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema tsibulnik
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tsibulnik
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tsibulnik` DEFAULT CHARACTER SET utf8 ;
USE `tsibulnik` ;

-- -----------------------------------------------------
-- Table `tsibulnik`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tsibulnik`.`users` (
  `id_users` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `date` DATETIME NOT NULL,
  PRIMARY KEY (`id_users`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tsibulnik`.`admins`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tsibulnik`.`admins` (
  `id_admin` INT(11) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  `author_id` INT(11) NOT NULL,
  `date` DATETIME NOT NULL,
  PRIMARY KEY (`id_admin`),
  INDEX `fk_admins_admins1_idx` (`author_id` ASC),
  CONSTRAINT `fk_admins_admins1`
    FOREIGN KEY (`author_id`)
    REFERENCES `tsibulnik`.`admins` (`id_admin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tsibulnik`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tsibulnik`.`category` (
  `id_category` INT(11) NOT NULL AUTO_INCREMENT,
  `name_category` VARCHAR(45) NOT NULL,
  `author_id` INT(11) NOT NULL,
  PRIMARY KEY (`id_category`),
  INDEX `fk_category_admins1_idx` (`author_id` ASC),
  CONSTRAINT `fk_category_admins1`
    FOREIGN KEY (`author_id`)
    REFERENCES `tsibulnik`.`admins` (`id_admin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tsibulnik`.`question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tsibulnik`.`question` (
  `id_question` INT(11) NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(255) NOT NULL,
  `author_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  `date` DATETIME NOT NULL,
  PRIMARY KEY (`id_question`),
  INDEX `fk_question_users_idx` (`author_id` ASC),
  INDEX `fk_question_category1_idx` (`category_id` ASC),
  CONSTRAINT `fk_question_users`
    FOREIGN KEY (`author_id`)
    REFERENCES `tsibulnik`.`users` (`id_users`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `tsibulnik`.`category` (`id_category`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tsibulnik`.`answer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tsibulnik`.`answer` (
  `id_answer` INT(11) NOT NULL AUTO_INCREMENT,
  `text_ans` VARCHAR(255) NOT NULL,
  `quistion_id` INT(11) NOT NULL,
  `date` DATETIME NOT NULL,
  `author_id` INT(11) NOT NULL,
  PRIMARY KEY (`id_answer`),
  INDEX `fk_answer_question1_idx` (`quistion_id` ASC),
  INDEX `fk_answer_admins1_idx` (`author_id` ASC),
  CONSTRAINT `fk_answer_question1`
    FOREIGN KEY (`quistion_id`)
    REFERENCES `tsibulnik`.`question` (`id_question`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_answer_admins1`
    FOREIGN KEY (`author_id`)
    REFERENCES `tsibulnik`.`admins` (`id_admin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
