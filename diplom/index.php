<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once __DIR__ . '/vendor/autoload.php';
$view=__DIR__.'/view';
Twig_Autoloader::register();
try
{
    $loader = new Twig_Loader_Filesystem($view);
    $twig = new Twig_Environment($loader);
    $template = $twig->loadTemplate('index.twig');
	 $params=['name' => '$v[]'];
    echo $template->render($params);
} catch (Exception $e)
{

    die('ERROR: ' . $e->getMessage());
}
	