<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Список файлов</title>
    </head>
    <style>
        body{margin: 50px auto;}
        h1{text-align: center;}
        table{margin: 30px auto;}
        th{padding: 20px;text-align: center;}
        td{padding: 15px;text-align: center;}
    </style>
    <body>
        <h1>Выберите картинку</h1>
        <hr>
        <table>
            <tr>
                <th>Предпросмотр</th>
                <th>Имя файла</th>
                <th>Размер файла</th>
                <th>Дата изменения</th>
            </tr>
<?php
error_reporting('E_ALL');
include 'scan_img.php';

$row = 1;
if (($handle = fopen("list_file.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        $num = count($data);
        $row++;?>
        <tr >
            <!--делаем превью картники</-->
            <?php
                $width = 100;
                $height = 100;
                $name_preview = str_replace(".jpg","_small.jpg", $data[0]);
                mkdir("img/small");
                $path_preview = "img/small/".$name_preview;
                $path_pic = "img/$data[0]";
                
                list($width_orig, $height_orig) = getimagesize($path_pic);
                
                
                $ratio_orig = $width_orig/$height_orig;
                
                if ($width/$height > $ratio_orig) {
                   $width = $height*$ratio_orig;
                   
                } else {
                   $height = $width/$ratio_orig;
                   
                }
                header ('Content-Type; image/jpeg');
                $image_p = imagecreatetruecolor($width, $height);
                
                $image = imagecreatefromjpeg($path_pic);
               
                
                imagecopyresampled($image_p, $image, 0,0,0,0, $width, $height,$width_orig, $height_orig);
                imagejpeg($image_p, $path_preview);

            ?>
            <td><img src="<?=$path_preview?>" alt="Предпосмотр файла <?=$data[0]?>"> </td>
            <td><a href="<?php echo "img/$data[0]"?>"><?=$data[0]?></a></td>
        <?php for ($c=1; $c < $num; $c++) {
            ?>
                
            <td><?=$data[$c];?></td>
            
           <?php  }
       }
    }
    fclose($handle);
    unlink("list_file.csv");
?>
        </tr>
        </table>
        <hr>
    </body>
</html>