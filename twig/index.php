<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/controller/controller.php';
require_once __DIR__ . '/model/guestbook.php';
require_once __DIR__ . '/vendor/autoload.php';

$guestbook = new Guestbook($dbuser, $dbpass, $dbhost, $dbname);
$controller = new Controller();

$guestbook->last();
$list = $guestbook->answerDB;

$controller->display('index.phtml', $list);
echo $controller->template;
